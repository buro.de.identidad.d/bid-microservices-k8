--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 9.6.10

-- Started on 2018-11-22 13:12:55

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 187 (class 1259 OID 16558)
-- Name: role_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role_user (
    role_id bigint NOT NULL,
    user_id bigint NOT NULL,
    created_on timestamp with time zone NOT NULL,
    updated_on timestamp with time zone NOT NULL,
    version bigint NOT NULL
);


ALTER TABLE public.role_user OWNER TO postgres;

--
-- TOC entry 2142 (class 0 OID 16558)
-- Dependencies: 187
-- Data for Name: role_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.role_user (role_id, user_id, created_on, updated_on, version) VALUES (1, 1, '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);
INSERT INTO public.role_user (role_id, user_id, created_on, updated_on, version) VALUES (2, 2, '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);
INSERT INTO public.role_user (role_id, user_id, created_on, updated_on, version) VALUES (2, 3, '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);


--
-- TOC entry 2024 (class 2606 OID 16562)
-- Name: role_user role_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_user
    ADD CONSTRAINT role_user_pkey PRIMARY KEY (role_id, user_id);


-- Completed on 2018-11-22 13:12:55

--
-- PostgreSQL database dump complete
--

