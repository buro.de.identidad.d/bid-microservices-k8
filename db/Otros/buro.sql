-- Database: buro
-- DROP DATABASE buro;
CREATE DATABASE buro
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Spanish_Mexico.1252'
    LC_CTYPE = 'Spanish_Mexico.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
------------------------------------------------- OAUTH --------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------

-- Table: oauth.oauth_access_token
-- DROP TABLE oauth.oauth_access_token;
CREATE TABLE oauth.oauth_access_token
(
    token_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    token bytea,
    authentication_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    user_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    client_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    authentication bytea,
    refresh_token character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT oauth_access_token_pkey PRIMARY KEY (token_id)
)
WITH (OIDS = FALSE)
TABLESPACE pg_default;
ALTER TABLE oauth.oauth_access_token OWNER to postgres;


-- Table: oauth.oauth_client_details
-- DROP TABLE oauth.oauth_client_details;
CREATE TABLE oauth.oauth_client_details
(
    client_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    resource_ids character varying(255) COLLATE pg_catalog."default" NOT NULL,
    client_secret character varying(255) COLLATE pg_catalog."default" NOT NULL,
    scope character varying(255) COLLATE pg_catalog."default" NOT NULL,
    authorized_grant_types character varying(255) COLLATE pg_catalog."default" NOT NULL,
    web_server_redirect_uri character varying(255) COLLATE pg_catalog."default",
    authorities character varying(255) COLLATE pg_catalog."default",
    access_token_validity integer NOT NULL,
    refresh_token_validity integer NOT NULL,
    additional_information character varying(4096) COLLATE pg_catalog."default" NOT NULL,
    autoapprove character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT oauth_client_details_pkey PRIMARY KEY (client_id)
)
WITH (OIDS = FALSE)
TABLESPACE pg_default;
ALTER TABLE oauth.oauth_client_details OWNER to postgres;


-- Table: oauth.oauth_refresh_token
-- DROP TABLE oauth.oauth_refresh_token;
CREATE TABLE oauth.oauth_refresh_token
(
    token_id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    token bytea,
    authentication bytea,
    CONSTRAINT oauth_refresh_token_pkey PRIMARY KEY (token_id)
)
WITH (OIDS = FALSE)
TABLESPACE pg_default;
ALTER TABLE oauth.oauth_refresh_token OWNER to postgres;


-- Table: oauth.permission
-- DROP TABLE oauth.permission;
CREATE TABLE oauth.permission
(
    id bigint NOT NULL,
    name character varying(60) COLLATE pg_catalog."default" NOT NULL,
    created_on timestamp with time zone NOT NULL,
    updated_on timestamp with time zone NOT NULL,
    version bigint NOT NULL,
    CONSTRAINT permission_pkey PRIMARY KEY (id)
)
WITH (OIDS = FALSE)
TABLESPACE pg_default;
ALTER TABLE oauth.permission OWNER to postgres;


-- Table: oauth.permission_role
-- DROP TABLE oauth.permission_role;
CREATE TABLE oauth.permission_role
(
    permission_id bigint NOT NULL,
    role_id bigint NOT NULL,
    created_on timestamp with time zone NOT NULL,
    updated_on timestamp with time zone NOT NULL,
    version bigint NOT NULL,
    CONSTRAINT permission_role_pkey PRIMARY KEY (permission_id, role_id)
)
WITH (OIDS = FALSE)
TABLESPACE pg_default;
ALTER TABLE oauth.permission_role OWNER to postgres;


-- Table: oauth.role
-- DROP TABLE oauth.role;
CREATE TABLE oauth.role
(
    id bigint NOT NULL,
    name character varying(60) COLLATE pg_catalog."default" NOT NULL,
    created_on timestamp with time zone NOT NULL,
    updated_on timestamp with time zone NOT NULL,
    version bigint NOT NULL,
    CONSTRAINT role_pkey PRIMARY KEY (id)
)
WITH (OIDS = FALSE)
TABLESPACE pg_default;
ALTER TABLE oauth.role OWNER to postgres;


-- Table: oauth.role_user
-- DROP TABLE oauth.role_user;
CREATE TABLE oauth.role_user
(
    role_id bigint NOT NULL,
    user_id bigint NOT NULL,
    created_on timestamp with time zone NOT NULL,
    updated_on timestamp with time zone NOT NULL,
    version bigint NOT NULL,
    CONSTRAINT role_user_pkey PRIMARY KEY (role_id, user_id)
)
WITH (OIDS = FALSE)
TABLESPACE pg_default;
ALTER TABLE oauth.role_user OWNER to postgres;


-- Table: oauth."user"
-- DROP TABLE oauth."user";
CREATE TABLE oauth."user"
(
    id bigint NOT NULL,
    username character varying(24) COLLATE pg_catalog."default" NOT NULL,
    password character varying(200) COLLATE pg_catalog."default" NOT NULL,
    email character varying(255) COLLATE pg_catalog."default" NOT NULL,
    enabled bit(1) NOT NULL,
    account_expired bit(1) NOT NULL,
    credentials_expired bit(1) NOT NULL,
    account_locked bit(1) NOT NULL,
    created_on timestamp with time zone NOT NULL,
    updated_on timestamp with time zone NOT NULL,
    version bigint,
    CONSTRAINT user_pkey PRIMARY KEY (id)
)
WITH (OIDS = FALSE)
TABLESPACE pg_default;
ALTER TABLE oauth."user" OWNER to postgres;

----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
--------------------------------------------------- BID --------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


