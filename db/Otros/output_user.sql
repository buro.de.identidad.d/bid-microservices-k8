--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 9.6.10

-- Started on 2018-11-22 13:13:13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 185 (class 1259 OID 16545)
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id bigint NOT NULL,
    username character varying(24) NOT NULL,
    password character varying(200) NOT NULL,
    email character varying(255) NOT NULL,
    enabled bit(1) NOT NULL,
    account_expired bit(1) NOT NULL,
    credentials_expired bit(1) NOT NULL,
    account_locked bit(1) NOT NULL,
    created_on timestamp with time zone NOT NULL,
    updated_on timestamp with time zone NOT NULL,
    version bigint
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- TOC entry 2142 (class 0 OID 16545)
-- Dependencies: 185
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."user" (id, username, password, email, enabled, account_expired, credentials_expired, account_locked, created_on, updated_on, version) VALUES (1, 'admin', '{bcrypt}$2a$10$EOs8VROb14e7ZnydvXECA.4LoIhPOoFHKvVF/iBZ/ker17Eocz4Vi', 'admin@example.com', B'1', B'0', B'0', B'0', '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);
INSERT INTO public."user" (id, username, password, email, enabled, account_expired, credentials_expired, account_locked, created_on, updated_on, version) VALUES (2, 'user', '{bcrypt}$2a$10$EOs8VROb14e7ZnydvXECA.4LoIhPOoFHKvVF/iBZ/ker17Eocz4Vi', 'user@example.com', B'1', B'0', B'0', B'0', '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);
INSERT INTO public."user" (id, username, password, email, enabled, account_expired, credentials_expired, account_locked, created_on, updated_on, version) VALUES (3, 'UsrOCR17', '{bcrypt}$2a$10$bt1L960D6sjroa4KI3OoKuIA2ebDxmUycIoG5/9KvtTLoHxe0RmiW', 'user2@example.com', B'1', B'0', B'0', B'0', '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);


--
-- TOC entry 2024 (class 2606 OID 16552)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


-- Completed on 2018-11-22 13:13:13

--
-- PostgreSQL database dump complete
--

