--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 9.6.10

-- Started on 2018-11-22 13:12:36

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 186 (class 1259 OID 16553)
-- Name: role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role (
    id bigint NOT NULL,
    name character varying(60) NOT NULL,
    created_on timestamp with time zone NOT NULL,
    updated_on timestamp with time zone NOT NULL,
    version bigint NOT NULL
);


ALTER TABLE public.role OWNER TO postgres;

--
-- TOC entry 2142 (class 0 OID 16553)
-- Dependencies: 186
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.role (id, name, created_on, updated_on, version) VALUES (1, 'role_admin', '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);
INSERT INTO public.role (id, name, created_on, updated_on, version) VALUES (2, 'role_user', '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);


--
-- TOC entry 2024 (class 2606 OID 16557)
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


-- Completed on 2018-11-22 13:12:36

--
-- PostgreSQL database dump complete
--

