-- Table: oauth.oauth_client_details
INSERT INTO oauth.oauth_client_details (client_id, resource_ids, client_secret, scope, authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, refresh_token_validity, additional_information, autoapprove) VALUES ('tknespapp', 'mw/adminapp,ms/admin,ms/user,bid,tkn/esp', '{bcrypt}$2y$10$.p2HyMoy.OkPqbjUCON3EO0HEfcWU7aCAK.HCPcrCA5XfZBvGqZNS', 'role_user', 'password,refresh_token', NULL, NULL, 900000, 360000, '{}', NULL);
INSERT INTO oauth.oauth_client_details (client_id, resource_ids, client_secret, scope, authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, refresh_token_validity, additional_information, autoapprove) VALUES ('adminapp', 'mw/adminapp,ms/admin,ms/user,bid,tkn/esp', '{bcrypt}$2a$10$EOs8VROb14e7ZnydvXECA.4LoIhPOoFHKvVF/iBZ/ker17Eocz4Vi', 'role_admin,role_superadmin', 'authorization_code,password,refresh_token,implicit', NULL, NULL, 900000, 360000, '{}', NULL);
INSERT INTO oauth.oauth_client_details (client_id, resource_ids, client_secret, scope, authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, refresh_token_validity, additional_information, autoapprove) VALUES ('userapp', 'mw/adminapp,ms/admin,ms/user,bid,tkn/esp', '{bcrypt}$2a$10$EOs8VROb14e7ZnydvXECA.4LoIhPOoFHKvVF/iBZ/ker17Eocz4Vi', 'role_user', 'password,refresh_token', NULL, NULL, 900000, 360000, '{}', NULL);

-- Table: oauth.permission
INSERT INTO oauth.permission (id, name, created_on, updated_on, version) VALUES (1, 'can_delete_user', '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);
INSERT INTO oauth.permission (id, name, created_on, updated_on, version) VALUES (2, 'can_create_user', '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);
INSERT INTO oauth.permission (id, name, created_on, updated_on, version) VALUES (3, 'can_update_user', '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);
INSERT INTO oauth.permission (id, name, created_on, updated_on, version) VALUES (4, 'can_read_user', '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);

-- Table: oauth.permission_role
INSERT INTO oauth.permission_role (permission_id, role_id, created_on, updated_on, version) VALUES (1, 1, '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);
INSERT INTO oauth.permission_role (permission_id, role_id, created_on, updated_on, version) VALUES (2, 1, '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);
INSERT INTO oauth.permission_role (permission_id, role_id, created_on, updated_on, version) VALUES (3, 1, '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);
INSERT INTO oauth.permission_role (permission_id, role_id, created_on, updated_on, version) VALUES (3, 2, '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);
INSERT INTO oauth.permission_role (permission_id, role_id, created_on, updated_on, version) VALUES (4, 1, '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);

-- Table: oauth.role
INSERT INTO oauth.role (id, name, created_on, updated_on, version) VALUES (1, 'role_admin', '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);
INSERT INTO oauth.role (id, name, created_on, updated_on, version) VALUES (2, 'role_user', '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);


-- Table: oauth.role_user
INSERT INTO oauth.role_user (role_id, user_id, created_on, updated_on, version) VALUES (1, 1, '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);
INSERT INTO oauth.role_user (role_id, user_id, created_on, updated_on, version) VALUES (2, 2, '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);
INSERT INTO oauth.role_user (role_id, user_id, created_on, updated_on, version) VALUES (2, 3, '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);

-- Table: oauth."user"
INSERT INTO oauth."user" (id, username, password, email, enabled, account_expired, credentials_expired, account_locked, created_on, updated_on, version) VALUES (1, 'admin', '{bcrypt}$2a$10$EOs8VROb14e7ZnydvXECA.4LoIhPOoFHKvVF/iBZ/ker17Eocz4Vi', 'admin@example.com', B'1', B'0', B'0', B'0', '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);
INSERT INTO oauth."user" (id, username, password, email, enabled, account_expired, credentials_expired, account_locked, created_on, updated_on, version) VALUES (2, 'user', '{bcrypt}$2a$10$EOs8VROb14e7ZnydvXECA.4LoIhPOoFHKvVF/iBZ/ker17Eocz4Vi', 'user@example.com', B'1', B'0', B'0', B'0', '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);
INSERT INTO oauth."user" (id, username, password, email, enabled, account_expired, credentials_expired, account_locked, created_on, updated_on, version) VALUES (3, 'UsrOCR17', '{bcrypt}$2a$10$bt1L960D6sjroa4KI3OoKuIA2ebDxmUycIoG5/9KvtTLoHxe0RmiW', 'user2@example.com', B'1', B'0', B'0', B'0', '1970-01-01 00:00:00-06', '1970-01-01 00:00:00-06', 0);





