package com.teknei.bid.auth.configuration;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.authentication.AbstractAuthenticationTargetUrlRequestHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import com.teknei.bid.auth.service.CustomUserDetailsService;

@Component
public class CustomLogoutSuccessHandler extends AbstractAuthenticationTargetUrlRequestHandler implements LogoutSuccessHandler {

	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(CustomLogoutSuccessHandler.class);

	private static final String BEARER_AUTHENTICATION = "Bearer ";
	private static final String HEADER_AUTHORIZATION = "authorization";

	@Autowired
	private TokenStore tokenStore;

	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		logger.info("CustomLogoutSuccessHandler --->> onLogoutSuccess --->> HEADER_AUTHORIZATION >>>>>  " + request.getHeader(HEADER_AUTHORIZATION));
		
		String token = request.getHeader(HEADER_AUTHORIZATION);
		if (token != null && token.startsWith(BEARER_AUTHENTICATION)) {
			
			String jwt = token.substring("Bearer".length() + 1);
			OAuth2AccessToken oAuth2AccessToken = tokenStore.readAccessToken(jwt);
			if (oAuth2AccessToken != null) {
				tokenStore.removeAccessToken(oAuth2AccessToken);
			}
		}
		response.setStatus(HttpServletResponse.SC_OK);
	}
}
