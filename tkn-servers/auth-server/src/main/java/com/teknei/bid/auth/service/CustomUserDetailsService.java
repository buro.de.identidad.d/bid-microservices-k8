package com.teknei.bid.auth.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.teknei.bid.auth.entity.User;
import com.teknei.bid.auth.repository.UserRepository;

@Service(value = "userDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(CustomUserDetailsService.class);
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String input) {
		logger.info("CustomUserDetailsService --->> loadUserByUsername --->> input >>>>>  " + input);
		
		Optional<User> user = null;
		
		if (input.contains("@")) {
			user = userRepository.findByEmail(input);
		}else {
			user = userRepository.findByUsername(input);			
			logger.info(" >>>>> Username ----->>> " +user.get().getUsername());
			logger.info(" >>>>> Password ----->>> " +user.get().getPassword());
			logger.info(" >>>>> Authorities ----->>> " +user.get().getAuthorities());
		}

		if (!user.isPresent())
			throw new BadCredentialsException("Bad credentials");

		new AccountStatusUserDetailsChecker().check(user.get());

		return user.get();
	}

}
