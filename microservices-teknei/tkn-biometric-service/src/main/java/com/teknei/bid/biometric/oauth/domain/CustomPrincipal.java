package com.teknei.bid.biometric.oauth.domain;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class CustomPrincipal implements Serializable {

	private static final long serialVersionUID = 1L;

	private String username;
	private String email;

	public CustomPrincipal() {}
	
	public CustomPrincipal(String username, String email) {
		this.username = username;
		this.email = email;
	}
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

}
