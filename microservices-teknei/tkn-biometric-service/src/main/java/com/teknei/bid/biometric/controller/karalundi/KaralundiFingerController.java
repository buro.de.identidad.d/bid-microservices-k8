package com.teknei.bid.biometric.controller.karalundi;


import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.context.ServletWebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.teknei.bid.biometric.data.karalundi.RequestKaralundi;
import com.teknei.bid.biometric.data.karalundi.ResponseKaralundi;
import com.teknei.bid.biometric.service.KaralundiFingerService;

@RestController
@RequestMapping(value = "/rest/v1/test/enrollment/finger")
public class KaralundiFingerController implements ApplicationListener<ServletWebServerInitializedEvent>{
	
	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(KaralundiFingerController.class);

	@Autowired
	private KaralundiFingerService karalundiFingerervice;

    @RequestMapping(value = "/startSession", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<ResponseKaralundi> startSession(HttpServletRequest request) {
		logger.info("<<< Start Session >>>");
		return new ResponseEntity<ResponseKaralundi>(karalundiFingerervice.startSession("finger_cfg_1", "5a5e22c849521", ""), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/endSession", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<ResponseKaralundi> endSession(HttpServletRequest request,
    		@RequestPart(value = "session") String session) {
		logger.info("<<< End Session >>>");
		
    	RequestKaralundi requestParam = new RequestKaralundi();
    	try {
    		requestParam.setSession(session);
		} catch (Exception ex) {
			logger.error("Ocurrió un error al terminar la sesión", ex);
		}
		return new ResponseEntity<ResponseKaralundi>(karalundiFingerervice.endSession(requestParam), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/enroll", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ResponseKaralundi> enroll(
    		@RequestPart(value = "session") String session,
    		@RequestPart(value = "speaker") String speaker,
            @RequestPart(value = "file", required = true) MultipartFile file,
            @RequestPart(value = "data") String data){
    	logger.info("Enroll ----->>> Session: "+session+ "  Speaker: "+speaker + " file:"+file.getOriginalFilename()+ " data:"+data);
		
    	RequestKaralundi requestParam = new RequestKaralundi();
    	try {
    		requestParam.setSession(session);
    		requestParam.setSpeaker(speaker);
    		requestParam.setData(data);
    		File convFile = new File(file.getOriginalFilename());
    		FileUtils.writeByteArrayToFile(convFile, file.getBytes());
			requestParam.setFile(convFile);
		} catch (Exception ex) {
			logger.error("Ocurrió un error en el servicio de finger enroll", ex);
		}

		return new ResponseEntity<ResponseKaralundi>(karalundiFingerervice.enrollMF(requestParam), HttpStatus.OK);
    }
    
	@Override
	public void onApplicationEvent(ServletWebServerInitializedEvent event) {
		logger.info(" >>>>> ApplicationName: " + event.getApplicationContext().getApplicationName());
		logger.info(" >>>>> Timestamp:       " + event.getTimestamp());
		logger.info(" >>>>> Source Port:     " + event.getSource().getPort());
		logger.info(" >>>>> WebSever Port:   " + event.getWebServer().getPort());	
	}

}
