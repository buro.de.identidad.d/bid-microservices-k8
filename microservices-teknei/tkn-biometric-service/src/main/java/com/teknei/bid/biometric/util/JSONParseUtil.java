package com.teknei.bid.biometric.util;

import com.google.gson.Gson;
import com.teknei.bid.biometric.data.karalundi.ResponseKaralundi;

public class JSONParseUtil {

	public static ResponseKaralundi stringToResponseKaralundi(String value) {
		if(value == null)
			return null;
		
		return new Gson().fromJson(value, ResponseKaralundi.class);
	}
	
}
