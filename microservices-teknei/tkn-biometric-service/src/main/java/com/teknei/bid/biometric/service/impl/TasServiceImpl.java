package com.teknei.bid.biometric.service.impl;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.teknei.bid.biometric.service.TasService;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URL;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TasServiceImpl implements TasService{

	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(TasServiceImpl.class);

    private static final String APISERVICE_DOCUMENTS_OPERATION_NAME = "/documents";
    private static final String APISERVICE_FILES_OPERATION_NAME = "/files";

    private static final String PARAMETER_DOCTYPE = "docType";
    private static final String PARAMETER_HEAD_DOCS = "headDocs";
    private static final String PARAMETER_TAIL_DOCS = "tailDocs";
    private static final String PARAMETER_FILES_FILE = "file";
    private static final String PARAMETER_FILES_FILENAME = "fileName";
    private static final String PARAMETER_FILES_CONTENTTYPE = "contentType";
    
    @Value("${tkn.tas.user}")
    private String tasUser;
    @Value("${tkn.tas.password}")
    private String tasPassword;
    @Value("${tkn.tas.url}")
    private String tasUrl;
    @Value("${tkn.tas.anverse}")
    private String tknTasAnverse;
    @Value("${tkn.tas.face}")
    private String tknTasFace;
    @Value("${tkn.tas.address}")
    private String tknTasAddress;
    @Value("${tkn.tas.name}")
    private String tasName;
    @Value("${tkn.tas.surname}")
    private String tasSurname;
    @Value("${tkn.tas.lastname}")
    private String tasLastname;
    @Value("${tkn.tas.identification}")
    private String tasIdentification;
    @Value("${tkn.tas.date}")
    private String tasDate;
    @Value("${tkn.tas.scan}")
    private String tasScan;
    @Value("${tkn.tas.id}")
    private String tasOperationId;
    @Value("${tkn.tas.casefile}")
    private String tasCasefile;
    @Value("${tkn.tas.idType}")
    private String tasTypeId;
    @Value("${tkn.tas.address}")
    private String tasAddressFile;
    
    @Override
    public JSONObject createDocument(
    		String operationId,
            String docType,
            String headDocs,
            String tailDocs,
            String bidNombre,
            String bidPrimerApellido,
            String bidSegundoApellido) {
    	
    	JSONObject jsonResponse = null;
    	try {
    		String autStr = tasUser + ":" + tasPassword;
            String authorization = new String( Base64Utils.encodeToString(new String(autStr).getBytes()));
            
    		RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "Basic " + authorization);
            
            MultiValueMap<String, Object> requestParameters = new LinkedMultiValueMap<String, Object>();
            requestParameters.add(PARAMETER_DOCTYPE, docType);
            requestParameters.add(PARAMETER_HEAD_DOCS, headDocs);
            requestParameters.add(PARAMETER_TAIL_DOCS, tailDocs);
            
            requestParameters.add(tasName, bidNombre);
            requestParameters.add(tasSurname, bidPrimerApellido);
            requestParameters.add(tasLastname, bidSegundoApellido);
            requestParameters.add(tasIdentification, String.valueOf(operationId));
            String dateISO8601 = ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT).toString();
            requestParameters.add("bid:Fecha", dateISO8601);
            
            String URL = new URL(String.format("%s%s", tasUrl, APISERVICE_DOCUMENTS_OPERATION_NAME)).toURI().toString();
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<MultiValueMap<String, Object>>(requestParameters, headers);
            ResponseEntity<String> response = restTemplate.exchange(URL, HttpMethod.POST, requestEntity, String.class);
            
            jsonResponse = new JSONObject(response.getBody().toString());
            /*if (jsonResponse.getBoolean("created") == true) {
            	response = jsonResponse.getString("id");
            }*/
    	}catch(Exception ex) {
    		logger.error("Ocurrió un error al crear el documento.", ex);
    	}
    	return jsonResponse;
    }
    
    @Override
    public JSONObject addFile(String documentId, String fileName, MultipartFile file, String contentType) throws Exception {
    logger.info(" >>> DocumentId: " +documentId+ " FileName: " +fileName+ " ContentType: " +contentType);
    
    ResponseEntity<String> response = null;
    JSONObject jsonResponse = null;
    
    try {
    	byte[] docBytes = file.getBytes();
    	
        if (docBytes != null && docBytes.length > 0) {
        	RestTemplate restTemplate = new RestTemplate();
        	MultiValueMap<String, Object> requestParameters = new LinkedMultiValueMap<String, Object>();
        	
            requestParameters = new LinkedMultiValueMap<>();
            requestParameters.add(PARAMETER_FILES_FILE, new ByteArrayResource(docBytes, fileName) {
            	@Override
            	public String getFilename() {
            		return this.getDescription();
            	}
            });
                
            requestParameters.add(PARAMETER_FILES_FILENAME, fileName);
            requestParameters.add(PARAMETER_FILES_CONTENTTYPE, contentType);
            HttpHeaders headers = new HttpHeaders();
            String autStr = tasUser + ":" + tasPassword;
            String authorization = new String( Base64Utils.encodeToString(new String(autStr).getBytes()));
            headers.add("Authorization", "Basic " + authorization);
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(requestParameters, headers);


            logger.info("   contentType::: "+contentType);
            logger.info("   fileName:::::: "+fileName);
            logger.info("   autStr:::::::: "+autStr);



             /*    
            String URL = new URL(String.format("%s%s/%s", tasUrl, APISERVICE_FILES_OPERATION_NAME, documentId)).toURI().toString();
            response = restTemplate.exchange(URL, HttpMethod.POST, requestEntity, String.class);
            
            jsonResponse = new JSONObject(response.getBody().toString());
            if (jsonResponse.getBoolean("file_added") == true) {
            	logger.info("Archivo subido correctamente. UUID: " + documentId);
            } else {
            	logger.error("Error al insertar el archivo para el UUID: " + documentId);            	
            }
          */
        }
    } catch (Exception e) {
        logger.error("Ocurrió un error al subir el archivo", e);
        throw e;
    }
    return jsonResponse;
   }
    
}
