package com.teknei.bid.biometric.service.impl;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.teknei.bid.biometric.data.karalundi.RequestKaralundi;
import com.teknei.bid.biometric.data.karalundi.ResponseKaralundi;
import com.teknei.bid.biometric.service.InvokerService;
import com.teknei.bid.biometric.service.KaralundiFaceService;
import com.teknei.bid.biometric.util.JSONParseUtil;
import com.teknei.bid.biometric.util.MultipartUtility;

@Component("karalundiFaceService")
@Transactional
public class KaralundiFaceServiceImpl implements KaralundiFaceService {
	
	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(KaralundiFaceServiceImpl.class); 
	
	@Value("${tkn.karalundi.host}")
	private String hostFace;
	
	@Value("${tkn.karalundi.resource.face.startSesion}")
	private String rscFaceStartSession;
	@Value("${tkn.karalundi.resource.face.enrollment}")
	private String rscFaceEnrolment;
	@Value("${tkn.karalundi.resource.face.verifyMF}")
	private String rscFaceVerify;
	@Value("${tkn.karalundi.resource.face.isTrained}")
	private String rscFaceIsTrained;
	@Value("${tkn.karalundi.resource.face.endSession}")
	private String rscFaceEndSession;
	@Value("${tkn.karalundi.resource.face.deleteSpareSegments}")
	private String rscFaceDeleteSpareSegments;
	@Value("${tkn.karalundi.resource.face.identity}")
	private String rscFaceIdentity;
	
	@Autowired
	private InvokerService invokerService;

	@Override
	public ResponseKaralundi startSession(String configuration, String enterpriseId, String device) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("configuration", configuration);
		params.put("enterpriseId", enterpriseId);
		params.put("device", device);
		return requestKaralundi(hostFace + rscFaceStartSession, params);
	}

	@Override
	public ResponseKaralundi enrollMF(RequestKaralundi requestKaralundi) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", requestKaralundi.getSession());
		params.put("speaker", requestKaralundi.getSpeaker());
		params.put("param", requestKaralundi.getParam());
		return requestMultipartKaralundi(hostFace + rscFaceEnrolment, params, requestKaralundi.getFile());
	}

	@Override
	public ResponseKaralundi verifyMF(RequestKaralundi requestKaralundi) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", requestKaralundi.getSession());
		params.put("speaker", requestKaralundi.getSpeaker());
		params.put("param", requestKaralundi.getParam());
		params.put("file", requestKaralundi.getFile());
		return requestMultipartKaralundi(hostFace + rscFaceVerify, params, requestKaralundi.getFile());
	}

	@Override
	public ResponseKaralundi isTrained(RequestKaralundi requestKaralundi) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", requestKaralundi.getSession());
		params.put("speaker", requestKaralundi.getSpeaker());
		params.put("param", requestKaralundi.getParam());
		return requestKaralundi(hostFace + rscFaceIsTrained, params);
	}

	@Override
	public ResponseKaralundi endSession(RequestKaralundi requestKaralundi) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", requestKaralundi.getSession());
		return requestKaralundi(hostFace + rscFaceEndSession, params);
	}

	@Override
	public ResponseKaralundi deleteSegments(RequestKaralundi requestKaralundi) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", requestKaralundi.getSession());
		params.put("speaker", requestKaralundi.getSpeaker());
		params.put("param", requestKaralundi.getParam());
		return requestKaralundi(hostFace + rscFaceDeleteSpareSegments, params);
	}

	@Override
	public ResponseKaralundi identifyMF(RequestKaralundi requestKaralundi) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", requestKaralundi.getSession());
		params.put("list", requestKaralundi.getList());
		params.put("param", requestKaralundi.getParam());
		params.put("file", requestKaralundi.getFile());
		params.put("channel", requestKaralundi.getChannel());
		return requestKaralundi(hostFace + rscFaceIdentity, params);
	}

	private ResponseKaralundi requestKaralundi(String url, Map<String,Object> params) {
		String response = invokerService.send(url, params);
		ResponseKaralundi responseKaralundi = null;
		
		if(response != null)
			responseKaralundi = JSONParseUtil.stringToResponseKaralundi(response);
		return responseKaralundi;
	}
	
	private ResponseKaralundi requestMultipartKaralundi(String url, Map<String,Object> params, File file) {
		ResponseKaralundi responseKaralundi = null;

		String response = null;
		try {
			MultipartUtility multipart = new MultipartUtility(url);
			multipart.addFormFieldMap(params);
			multipart.addFilePart("file", file);
			List<String> lstResponse = multipart.finish();
			response = lstResponse != null ? lstResponse.get(0) : null;
		} catch (Exception ex) {
			logger.error("Ocurrió un error al realizar la petición al servicio.", ex);
		}
		
		if(response != null)
			responseKaralundi = JSONParseUtil.stringToResponseKaralundi(response);
		return responseKaralundi;
	}
}
