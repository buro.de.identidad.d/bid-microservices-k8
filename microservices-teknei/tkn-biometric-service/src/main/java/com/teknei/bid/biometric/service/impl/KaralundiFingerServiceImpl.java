package com.teknei.bid.biometric.service.impl;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.teknei.bid.biometric.data.karalundi.RequestKaralundi;
import com.teknei.bid.biometric.data.karalundi.ResponseKaralundi;
import com.teknei.bid.biometric.service.InvokerService;
import com.teknei.bid.biometric.service.KaralundiFingerService;
import com.teknei.bid.biometric.util.JSONParseUtil;
import com.teknei.bid.biometric.util.MultipartUtility;

@Component("karalundiFingerService")
@Transactional
public class KaralundiFingerServiceImpl implements KaralundiFingerService {

	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(KaralundiFingerServiceImpl.class); 

	@Value("${tkn.karalundi.host}")
	private String hostFinger;
	
	@Value("${tkn.karalundi.resource.finger.startSesion}")
	private String rscFingerStartSession;
	@Value("${tkn.karalundi.resource.finger.enrollment}")
	private String rscFingerEnrolment;
	@Value("${tkn.karalundi.resource.finger.disenroll}")
	private String rscFingerDisenroll;
	@Value("${tkn.karalundi.resource.finger.verifyMF}")
	private String rscFingerVerify;
	@Value("${tkn.karalundi.resource.finger.isTrained}")
	private String rscFingerIsTrained;
	@Value("${tkn.karalundi.resource.finger.endSession}")
	private String rscFingerEndSession;
	@Value("${tkn.karalundi.resource.finger.deleteSpareSegments}")
	private String rscFingerDeleteSpareSegments;
	@Value("${tkn.karalundi.resource.finger.identity}")
	private String rscFingerIdentity;
	@Value("${tkn.karalundi.resource.finger.update}")
	private String rscFingerUpdate;
	

	@Autowired
	private InvokerService invokerService;
	
	@Override
	public ResponseKaralundi startSession(String configuration, String enterpriseId, String device) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("configuration", configuration);
		params.put("enterpriseId", enterpriseId);
		return requestKaralundi(hostFinger + rscFingerStartSession, params);
	}

	@Override
	public ResponseKaralundi endSession(RequestKaralundi request) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", request.getSession());
		return requestKaralundi(hostFinger + rscFingerEndSession, params);
	}

	@Override
	public ResponseKaralundi isTrained(RequestKaralundi request) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", request.getSession());
		params.put("speaker", request.getSpeaker());
		return requestKaralundi(hostFinger + rscFingerIsTrained, params);
	}

	@Override
	public ResponseKaralundi enrollMF(RequestKaralundi request) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", request.getSession());
		params.put("speaker", request.getSpeaker());
		params.put("data", request.getData());
		return requestMultipartKaralundi(hostFinger + rscFingerEnrolment, params, request.getFile());
	}

	@Override
	public ResponseKaralundi disenroll(RequestKaralundi request) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", request.getSession());
		params.put("speaker", request.getSpeaker());
		params.put("image", request.getImage());
		return requestKaralundi(hostFinger + rscFingerDisenroll, params);
	}

	@Override
	public ResponseKaralundi deleteSegments(RequestKaralundi request) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", request.getSession());
		params.put("speaker", request.getSpeaker());
		params.put("image", request.getImage());
		return requestKaralundi(hostFinger + rscFingerDeleteSpareSegments, params);
	}

	@Override
	public ResponseKaralundi verifyMF(RequestKaralundi requestKaralundi) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", requestKaralundi.getSession());
		params.put("speaker", requestKaralundi.getSpeaker());
		
		
		return requestMultipartKaralundi(hostFinger + rscFingerVerify, params, requestKaralundi.getMapFiles());
	}

	@Override
	public ResponseKaralundi identifyMF(RequestKaralundi request) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", request.getSession());
		params.put("speaker", request.getSpeaker());
		params.put("param", request.getParam());

		params.put("left-index", request.getLeftIndex());
		params.put("left-middle", request.getLeftMiddle());
		params.put("left-ring", request.getLeftRing());
		params.put("left-little", request.getLeftLittle());
		params.put("left-thumb", request.getLeftThumb());
		
		params.put("right-index", request.getRightIndex());
		params.put("right-middle", request.getRightMiddle());
		params.put("right-ring", request.getRightRing());
		params.put("right-little", request.getRightLittle());
		params.put("right-thumb", request.getRightThumb());
		
		return requestKaralundi(hostFinger + rscFingerIdentity, params);
	}

	@Override
	public ResponseKaralundi updateMF(RequestKaralundi request) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", request.getSession());
		params.put("speaker", request.getSpeaker());
		params.put("param", request.getParam());

		params.put("left-index", request.getLeftIndex());
		params.put("left-middle", request.getLeftMiddle());
		params.put("left-ring", request.getLeftRing());
		params.put("left-little", request.getLeftLittle());
		params.put("left-thumb", request.getLeftThumb());
		
		params.put("right-index", request.getRightIndex());
		params.put("right-middle", request.getRightMiddle());
		params.put("right-ring", request.getRightRing());
		params.put("right-little", request.getRightLittle());
		params.put("right-thumb", request.getRightThumb());
		
		return requestKaralundi(hostFinger + rscFingerUpdate, params);
	}

	private ResponseKaralundi requestKaralundi(String url, Map<String,Object> params) {
		String response = invokerService.send(url, params);
		ResponseKaralundi responseKaralundi = null;
		if(response != null)
			responseKaralundi = JSONParseUtil.stringToResponseKaralundi(response);
		return responseKaralundi;
	}
	
	private ResponseKaralundi requestMultipartKaralundi(String url, Map<String,Object> params, File file) {
		ResponseKaralundi responseKaralundi = null;

		String response = null;
		try {
			MultipartUtility multipart = new MultipartUtility(url);
			multipart.addFormFieldMap(params);
			
			boolean contains = file.getName().contains("WSQ");
			
			multipart.addFilePart("file", file);
			
			List<String> lstResponse = multipart.finish();
			if(lstResponse != null && !lstResponse.isEmpty()) {
				response = lstResponse.get(0);
				logger.info(" >>> Size Response: " + lstResponse.size());
				logger.info(" >>> RESPONSE " + response);
			}
			
			//response = lstResponse != null ? lstResponse.get(0) : null;
		} catch (Exception ex) {
			logger.error("Ocurrió un error al realizar la petición al servicio.", ex);
		}
		
		if(response != null)
			responseKaralundi = JSONParseUtil.stringToResponseKaralundi(response);
		return responseKaralundi;
	}
	
	private ResponseKaralundi requestMultipartKaralundi(String url, Map<String,Object> params, Map<String,File> files) {
		ResponseKaralundi responseKaralundi = null;

		String response = null;
		try {
			MultipartUtility multipart = new MultipartUtility(url);
			multipart.addFormFieldMap(params);
			
			for(Map.Entry<String,File> entry: files.entrySet()){
				multipart.addFilePart(entry.getKey(),entry.getValue());
			}
			
			List<String> lstResponse = multipart.finish();
			if(lstResponse != null && !lstResponse.isEmpty()) {
				response = lstResponse.get(0);
				logger.info(" >>> Size Response: " + lstResponse.size());
				logger.info(" >>> RESPONSE " + response);
			}
			
			//response = lstResponse != null ? lstResponse.get(0) : null;
		} catch (Exception ex) {
			logger.error("Ocurrió un error al realizar la petición al servicio.", ex);
		}
		
		if(response != null)
			responseKaralundi = JSONParseUtil.stringToResponseKaralundi(response);
		return responseKaralundi;
	}
}
