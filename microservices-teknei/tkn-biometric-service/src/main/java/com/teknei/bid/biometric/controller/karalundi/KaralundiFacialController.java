package com.teknei.bid.biometric.controller.karalundi;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.context.ServletWebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.teknei.bid.biometric.data.karalundi.RequestKaralundi;
import com.teknei.bid.biometric.data.karalundi.ResponseKaralundi;
import com.teknei.bid.biometric.service.KaralundiFaceService;

@RestController
@RequestMapping(value = "/rest/v1/test/enrollment/facial")
public class KaralundiFacialController implements ApplicationListener<ServletWebServerInitializedEvent>{

	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(KaralundiFacialController.class);
	
	@Autowired
	private KaralundiFaceService karalundiFaceService;
	
    @RequestMapping(value = "/startSession", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<ResponseKaralundi> startSession(HttpServletRequest request) {
		logger.info("<<< Start Session >>>");
		return new ResponseEntity<ResponseKaralundi>(karalundiFaceService.startSession("config_facial", "5bd0917354a78", ""), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/endSession", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<ResponseKaralundi> endSession(HttpServletRequest request,
    		@RequestPart(value = "session") String session) {
		logger.info("<<< End Session >>>");
		
    	RequestKaralundi requestParam = new RequestKaralundi();
    	try {
    		requestParam.setSession(session);
		} catch (Exception ex) {
			logger.error("Ocurrió un error al terminar la sesión", ex);
		}
		return new ResponseEntity<ResponseKaralundi>(karalundiFaceService.endSession(requestParam), HttpStatus.OK);
    }

    @RequestMapping(value = "/enroll", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ResponseKaralundi> enroll(
    		@RequestPart(value = "session") String session,
    		@RequestPart(value = "speaker") String speaker,
            @RequestPart(value = "file", required = true) MultipartFile file){
    	logger.info("Enroll ----->>> Session: "+session+ "  Speaker: "+speaker + " file:"+file.getOriginalFilename());
		
    	RequestKaralundi requestParam = new RequestKaralundi();
    	
    	try {
    		requestParam.setSession(session);
    		requestParam.setSpeaker(speaker);
    		
    		File convFile = new File(file.getOriginalFilename());
    		FileUtils.writeByteArrayToFile(convFile, file.getBytes());
			requestParam.setFile(convFile);
		} catch (Exception ex) {
			logger.error("Ocurrió un error en el servicio de facial enroll", ex);
		}
    	
		return new ResponseEntity<ResponseKaralundi>(karalundiFaceService.enrollMF(requestParam), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/verify", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ResponseKaralundi> verify(
    		@RequestPart(value = "session") String session,
    		@RequestPart(value = "speaker") String speaker,
            @RequestPart(value = "file", required = true) MultipartFile file){
    	logger.info("Verify ----->>> Session: "+session+ "  Speaker: "+speaker+ " file:"+file.getOriginalFilename());
		
    	RequestKaralundi requestParam = new RequestKaralundi();
    	
    	try {
    		requestParam.setSession(session);
    		requestParam.setSpeaker(speaker);
    		
    		File convFile = new File(file.getOriginalFilename());
    		FileUtils.writeByteArrayToFile(convFile, file.getBytes());
			requestParam.setFile(convFile);
		} catch (Exception ex) {
			logger.error("Ocurrió un error en el servicio de facial verify", ex);
		}
    	
		return new ResponseEntity<ResponseKaralundi>(karalundiFaceService.verifyMF(requestParam), HttpStatus.OK);
    }    
    
	@Override
	public void onApplicationEvent(ServletWebServerInitializedEvent event) {
		logger.info(" >>>>> ApplicationName: " + event.getApplicationContext().getApplicationName());
		logger.info(" >>>>> Timestamp:       " + event.getTimestamp());
		logger.info(" >>>>> Source Port:     " + event.getSource().getPort());
		logger.info(" >>>>> WebSever Port:   " + event.getWebServer().getPort());	
	} 

}