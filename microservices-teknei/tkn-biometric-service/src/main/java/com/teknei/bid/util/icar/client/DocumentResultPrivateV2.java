
package com.teknei.bid.util.icar.client;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DocumentResultPrivateV2.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;simpleType name="DocumentResultPrivateV2">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSet"/>
 *     &lt;enumeration value="Ok"/>
 *     &lt;enumeration value="NotCorrect"/>
 *     &lt;enumeration value="NotProcessed"/>
 *     &lt;enumeration value="Warning"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DocumentResultPrivateV2")
@XmlEnum
public enum DocumentResultPrivateV2 {

    @XmlEnumValue("NotSet")
    NOT_SET("NotSet"),
    @XmlEnumValue("Ok")
    OK("Ok"),
    @XmlEnumValue("NotCorrect")
    NOT_CORRECT("NotCorrect"),
    @XmlEnumValue("NotProcessed")
    NOT_PROCESSED("NotProcessed"),
    @XmlEnumValue("Warning")
    WARNING("Warning");
    private final String value;

    DocumentResultPrivateV2(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DocumentResultPrivateV2 fromValue(String v) {
        for (DocumentResultPrivateV2 c: DocumentResultPrivateV2 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
