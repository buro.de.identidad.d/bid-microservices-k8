package com.teknei.bid.biometric.controller;

import java.util.List;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.context.ServletWebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.teknei.bid.biometric.data.OperationResult;
import com.teknei.bid.biometric.service.TasService;
import com.teknei.bid.biometric.service.icar.IcarManager;

@RestController
@RequestMapping(value = "/rest/v1/enrollment/ocr")
public class OCRController implements ApplicationListener<ServletWebServerInitializedEvent>{

	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(OCRController.class);

    @Autowired
    private IcarManager icarManager;
    
    @Autowired
	private TasService tasService;
    
	@PreAuthorize("hasAuthority('role_user')")
    @RequestMapping(value = "/id", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<OperationResult> enroll(
            @RequestPart(value = "json") String json,
            @RequestPart(value = "file", required = false) List<MultipartFile> files){

    	logger.info("Enroll ----->>> JSON: "+json+ " file:"+files.size());
    	JSONObject jsonResult = null;
    	OperationResult operationResult = new OperationResult();
    	try {
	    	JSONObject jsonString = new JSONObject(json);
	        String operationId = jsonString.getString("operationId");
	        logger.info("OperationId: " +operationId);
	        
	        jsonResult = icarManager.getDocumentInfo(files, "image/jpeg", 500);
    	} catch (Exception ex) {
    		logger.error("Ocurrió un error en el servicio de finger enroll", ex);
    	}

        if (jsonResult == null) {
            operationResult.setResultOK(Boolean.FALSE);
            operationResult.setErrorMessage("10001");
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }

        try {
			//String code = jsonResult.getString("code");
			operationResult.setErrorMessage(jsonResult.toString());
			operationResult.setResultOK(Boolean.TRUE);
			return new ResponseEntity<OperationResult>(operationResult, HttpStatus.OK);
		} catch (Exception jsonex) {
			logger.error("Ocurrió un error al obtener el code de la respuesta", jsonex);
			operationResult.setErrorMessage("Ocurrió un error en la respuesta");
			operationResult.setResultOK(Boolean.FALSE);
			return new ResponseEntity<OperationResult>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
		}
    }
	
	@PreAuthorize("hasAuthority('role_user')")
    @RequestMapping(value = "/tas", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<OperationResult> tas(
            @RequestPart(value = "json") String json,
            @RequestPart(value = "file", required = false) List<MultipartFile> files){
    	logger.info(" ***** TAS ----->>> JSON: " +json+ " file:" +files.size());
    	
    	JSONObject jsonResult = null;
    	OperationResult operationResult = new OperationResult();
    	
    	try {
	    	JSONObject jsonString = new JSONObject(json);
	        String operationId = jsonString.getString("operationId");
	        String docType = jsonString.getString("docType");
	        String bidNombre = jsonString.getString("bidNombre");
	        String bidPrimerApellido = jsonString.getString("bidPrimerApellido");
	        String bidSegundoApellido = jsonString.getString("bidSegundoApellido");
	        String contentType = jsonString.getString("contentType");
	        
	        logger.info(" >>> OperationId: " +operationId+ " DocType: " +docType+ " BidNombre: " +bidNombre+ " BidPrimerApellido: " +
	        		bidPrimerApellido+ " BidSegundoApellido: " +bidSegundoApellido+ " ContentType: " +contentType);
	        
	        String response = "";
	        for(MultipartFile mFile : files) {
	        	JSONObject jsonDocument = tasService.createDocument(operationId, docType, "", "", bidNombre, bidPrimerApellido, bidSegundoApellido);
	        	String documentId = null;
	        	if (jsonDocument != null && jsonDocument.getBoolean("created") == true) {
	        		documentId = jsonDocument.getString("id");
	        		logger.info(" >>> documentId: " + documentId);
	        	}
	        	
	        	if(documentId != null) {
	        		jsonResult = tasService.addFile(documentId, "documento", mFile, contentType);
	        		response = response + " " + jsonResult.getBoolean("file_added");
	        	}
	        }
	        
			operationResult.setMessage(response);
			operationResult.setResultOK(Boolean.TRUE);
			return new ResponseEntity<OperationResult>(operationResult, HttpStatus.OK);
    	} catch (Exception ex) {
    		logger.error("Ocurrió un error en el servicio de TAS", ex);
			operationResult.setErrorMessage("Ocurrió un error en la respuesta");
			operationResult.setResultOK(Boolean.FALSE);
			return new ResponseEntity<OperationResult>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
    	}
    }

	@Override
	public void onApplicationEvent(ServletWebServerInitializedEvent event) {
	}
	
}
