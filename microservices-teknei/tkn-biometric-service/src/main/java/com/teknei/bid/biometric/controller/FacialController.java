package com.teknei.bid.biometric.controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.context.ServletWebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.teknei.bid.biometric.data.OperationResult;
import com.teknei.bid.biometric.data.karalundi.RequestKaralundi;
import com.teknei.bid.biometric.data.karalundi.ResponseKaralundi;
import com.teknei.bid.biometric.service.KaralundiFaceService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/rest/v1/enrollment/facial")
//@CrossOrigin
public class FacialController implements ApplicationListener<ServletWebServerInitializedEvent>{
	
	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(FacialController.class);
	
	@Autowired
	private KaralundiFaceService karalundiFaceService;
    

    @ApiOperation(value = "Stores the picture of the face for the case file. Expects the attachment and a JSON like {'operationId' : 1}", response = OperationResult.class)
    @RequestMapping(value = "/unuath/face", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<OperationResult> addFace_unauth(
            @RequestPart(value = "json") String jsonRequest,
            @RequestPart(value = "file") MultipartFile file) {
    	logger.info("Face ----->>> json: "+jsonRequest+ " file:"+file.getOriginalFilename());
        logger.info("Exito RAMON, Timon");

    	OperationResult operationResult = new OperationResult();
        try {
        	JSONObject credentialsOperationRequestJSON = new JSONObject(jsonRequest);
        	String operationId = credentialsOperationRequestJSON.getString("operationId");
        	logger.info(" OperationId: " + operationId);
        	
        	ResponseKaralundi respKara = karalundiFaceService.startSession("config_facial", "5bd0917354a78", "");
        	logger.info(" Session: " + respKara.getSession());
        	
        	RequestKaralundi requestParam = new RequestKaralundi();
    		requestParam.setSession(respKara.getSession());
    		requestParam.setSpeaker(String.valueOf(operationId));
    		
    		File convFile = new File(file.getOriginalFilename());
    		FileUtils.writeByteArrayToFile(convFile, file.getBytes());
			requestParam.setFile(convFile);
			
			respKara = karalundiFaceService.enrollMF(requestParam);
			
			String status = respKara.getStatus(); // FAIL, SUCCESS
			logger.info("Status: " + status);

			if(status != null && "SUCCESS".equals(respKara.getStatus().toUpperCase())) {
				logger.info("El enrolamiento fue exitoso");
				operationResult.setResultOK(true);
				operationResult.setMessage("Se ha almacenado el rostro en el expediente");
	            return new ResponseEntity<>(operationResult, HttpStatus.OK);
			}else {
				logger.error("Ocurrió un error al enrolar al usuario con OperationId: " + operationId);
				operationResult.setResultOK(false);
				operationResult.setErrorMessage("20001");
				return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
			}
        } catch (Exception ex) {
        	logger.error("Ocurrió un error al parsear el json", ex);
        	operationResult.setResultOK(false);
        	operationResult.setErrorMessage("20001");
            return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
        }
    }


      @PreAuthorize("hasAuthority('role_user')")
    @ApiOperation(value = "Stores the picture of the face for the case file. Expects the attachment and a JSON like {'operationId' : 1}", response = OperationResult.class)
    @RequestMapping(value = "/face", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public ResponseEntity<OperationResult> addFace(
            @RequestPart(value = "json") String jsonRequest,
            @RequestPart(value = "file") MultipartFile file) {
        logger.info("Face ----->>> json: "+jsonRequest+ " file:"+file.getOriginalFilename());

        OperationResult operationResult = new OperationResult();
        try {
                JSONObject credentialsOperationRequestJSON = new JSONObject(jsonRequest);
                String operationId = credentialsOperationRequestJSON.getString("operationId");
                logger.info(" OperationId: " + operationId);

                ResponseKaralundi respKara = karalundiFaceService.startSession("config_facial", "5bd0917354a78", "");
                logger.info(" Session: " + respKara.getSession());

                RequestKaralundi requestParam = new RequestKaralundi();
                requestParam.setSession(respKara.getSession());
                requestParam.setSpeaker(String.valueOf(operationId));

                File convFile = new File(file.getOriginalFilename());
                FileUtils.writeByteArrayToFile(convFile, file.getBytes());
                        requestParam.setFile(convFile);

                        respKara = karalundiFaceService.enrollMF(requestParam);
                        String status = respKara.getStatus(); // FAIL, SUCCESS
                        logger.info("Status: " + status);

                        if(status != null && "SUCCESS".equals(respKara.getStatus().toUpperCase())) {
                                logger.info("El enrolamiento fue exitoso");
                                operationResult.setResultOK(true);
                                operationResult.setMessage("Se ha almacenado el rostro en el expediente");
                    return new ResponseEntity<>(operationResult, HttpStatus.OK);
                        }else {
                                logger.error("Ocurrió un error al enrolar al usuario con OperationId: " + operationId);
                                operationResult.setResultOK(false);
                                operationResult.setErrorMessage("20001");
                                return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
                        }
        } catch (Exception ex) {
                logger.error("Ocurrió un error al parsear el json", ex);
                operationResult.setResultOK(false);
                operationResult.setErrorMessage("20001");
            return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
        }
    }














	@Override
	public void onApplicationEvent(ServletWebServerInitializedEvent event) {
	} 
}
