package com.teknei.bid.biometric;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.security.cert.X509Certificate;

public class Test {

	private static String ENDPOINT = "https://13.58.109.129";
	private static String RSC_START_SESSION = "/kivoxWeb/rest/face/start-session";
	private static String RSC_ENROLLMENT = "/kivoxWeb/rest/face/enroll-mf";
	
    public static void main(String[] args) {
    	init();
    	//startSession();
    	sendRestTemplate();
    }
    
    public static void sendRestTemplate() {
    	String url = "https://13.58.109.129/kivoxWeb/rest/face/start-session";

		// URI (URL) parameters
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("configuration", "config_facial");
		params.put("enterpriseId", "5bd0917354a78");
		params.put("device", "");

		// Query parameters
		UriComponentsBuilder builder = UriComponentsBuilder
			.fromUriString(url)
	        .queryParam("configuration", "config_facial")
	        .queryParam("enterpriseId", "5bd0917354a78")
	        .queryParam("device", "");
		
		/*URI uri = UriComponentsBuilder.fromUriString(url)
		        .buildAndExpand(params)
		        .toUri();
		
		uri = UriComponentsBuilder
			.fromUriString(url)
	        .queryParam("configuration", "config_facial")
	        .queryParam("enterpriseId", "5bd0917354a78")
	        .queryParam("device", "")
	        .build()
	        .toUri();
		*/
		
		System.out.println(builder.buildAndExpand(params).toUri());
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/x-www-form-urlencoded");
		//headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
			
		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<Object> requestEntity = new HttpEntity<>(headers);
		//HttpEntity<Object> requestEntity = new HttpEntity<>(new HttpHeaders());

		restTemplate.exchange(
			builder.buildAndExpand(params).toUri(),
			//uri,
			HttpMethod.POST,
			requestEntity,
			String.class
		);
    }
    
    public static void init() {
		try {
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {
					new X509TrustManager() {
						public java.security.cert.X509Certificate[] getAcceptedIssuers() {return null;}
						public void checkClientTrusted(X509Certificate[] certs, String authType) {}
						public void checkServerTrusted(X509Certificate[] certs, String authType) {}
					}
			};
			
			// Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			
			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};
			
			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (Exception e) {
			e.printStackTrace();
		}

    }
    
    public static void startSession()  {   	
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("configuration", "config_facial");
		params.put("enterpriseId", "5bd0917354a78");
		params.put("device", "");
	    
		String intentData = send(RSC_START_SESSION, params);
		System.out.println(intentData);	
    }
    
    public static void enrollMF() {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", "config_facial");
		params.put("speaker", "5bd0917354a78");
		params.put("file", "");
		params.put("file", "");
	    
		String intentData = send(RSC_ENROLLMENT, params);
		System.out.println(intentData);	
    }
    
    public static String send(String resource, Map<String,Object> params) {
    	String response = null;
    	
    	try {
            URL url = new URL(ENDPOINT + resource);

    		StringBuilder postData = new StringBuilder();
    		for (Map.Entry<String,Object> param : params.entrySet()) {
    			if (postData.length() != 0) postData.append('&');
    			postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
    			postData.append('=');
    			postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
    		}
    		
    		byte[] postDataBytes = postData.toString().getBytes("UTF-8");
    		
    		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
    		conn.setRequestMethod("POST");
    		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
    		conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
    		conn.setDoOutput(true);
    		conn.getOutputStream().write(postDataBytes);
    		
    		Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
    		StringBuilder data = new StringBuilder();
    		for (int c; (c = in.read()) >= 0;) {
    			data.append((char)c);
    		}
    		
    		response = data.toString();    		
    	}catch(IOException ioex) {
    		ioex.printStackTrace();
    	}
    	
    	return response;
    }
}
