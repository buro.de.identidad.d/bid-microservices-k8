package com.teknei.bid.biometric.data.tas;

public class TasDocument {

	private String docType;
	private String headDocs;
	private String tailDocs;
	private String bidNombre;
	private String bidPrimerApellido;
	private String bidSegundoApellido;
	private String bidIdentificacion;
	/**
	 * @return the docType
	 */
	public String getDocType() {
		return docType;
	}
	/**
	 * @param docType the docType to set
	 */
	public void setDocType(String docType) {
		this.docType = docType;
	}
	/**
	 * @return the headDocs
	 */
	public String getHeadDocs() {
		return headDocs;
	}
	/**
	 * @param headDocs the headDocs to set
	 */
	public void setHeadDocs(String headDocs) {
		this.headDocs = headDocs;
	}
	/**
	 * @return the tailDocs
	 */
	public String getTailDocs() {
		return tailDocs;
	}
	/**
	 * @param tailDocs the tailDocs to set
	 */
	public void setTailDocs(String tailDocs) {
		this.tailDocs = tailDocs;
	}
	/**
	 * @return the bidNombre
	 */
	public String getBidNombre() {
		return bidNombre;
	}
	/**
	 * @param bidNombre the bidNombre to set
	 */
	public void setBidNombre(String bidNombre) {
		this.bidNombre = bidNombre;
	}
	/**
	 * @return the bidPrimerApellido
	 */
	public String getBidPrimerApellido() {
		return bidPrimerApellido;
	}
	/**
	 * @param bidPrimerApellido the bidPrimerApellido to set
	 */
	public void setBidPrimerApellido(String bidPrimerApellido) {
		this.bidPrimerApellido = bidPrimerApellido;
	}
	/**
	 * @return the bidSegundoApellido
	 */
	public String getBidSegundoApellido() {
		return bidSegundoApellido;
	}
	/**
	 * @param bidSegundoApellido the bidSegundoApellido to set
	 */
	public void setBidSegundoApellido(String bidSegundoApellido) {
		this.bidSegundoApellido = bidSegundoApellido;
	}
	/**
	 * @return the bidIdentificacion
	 */
	public String getBidIdentificacion() {
		return bidIdentificacion;
	}
	/**
	 * @param bidIdentificacion the bidIdentificacion to set
	 */
	public void setBidIdentificacion(String bidIdentificacion) {
		this.bidIdentificacion = bidIdentificacion;
	}
}