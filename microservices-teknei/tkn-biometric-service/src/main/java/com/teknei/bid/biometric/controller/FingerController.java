package com.teknei.bid.biometric.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

import com.teknei.bid.biometric.data.OperationResult;
import com.teknei.bid.biometric.data.karalundi.RequestKaralundi;
import com.teknei.bid.biometric.data.karalundi.ResponseKaralundi;
import com.teknei.bid.biometric.service.KaralundiFingerService;

@RestController
@RequestMapping(value = "/rest/v1/enrollment/finger")
public class FingerController {

	private static Logger logger = LoggerFactory.getLogger(FingerController.class);

	@Autowired
	private KaralundiFingerService karalundiFingerervice;

	@RequestMapping(value = "/enroll", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
	public ResponseEntity<OperationResult> addMinuciasCyphered(HttpServletRequest request,
			@RequestPart(value = "json") String jsonRequest) {

		logger.info("enroll ----->>> json: " + jsonRequest);
		OperationResult operationResult = new OperationResult();

		try {
			JSONObject jsonString = new JSONObject(jsonRequest);
			String operationId = jsonString.getString("operationId");
			logger.info(" OperationId: " + operationId);

			ResponseKaralundi respKara = karalundiFingerervice.startSession("config_8", "5bd0917354a78", "");
			logger.info(" Session: " + respKara.getSession());

			RequestKaralundi requestParam = new RequestKaralundi();
			requestParam.setSession(respKara.getSession());
			requestParam.setSpeaker(operationId);

			@SuppressWarnings("unchecked")
			Iterator<String> iterator = jsonString.keys();

			while (iterator.hasNext()) {
				File convFile = null;
				String finger = "";
				String key = iterator.next();
				String encodedImg = "";

				if (!jsonString.getString(key).isEmpty())
					encodedImg = jsonString.getString(key);
				else
					continue;

				switch (key) {
				case "leftthumb":
					finger = "{\"fpg\":6}";
					convFile = new File(key + ".wsq");
					break;
				case "leftindex":
					finger = "{\"fpg\":7}";
					convFile = new File(key + ".wsq");
					break;
				case "leftmiddle":
					finger = "{\"fpg\":8}";
					convFile = new File(key + ".wsq");
					break;
				case "leftring":
					finger = "{\"fpg\":9}";
					convFile = new File(key + ".wsq");
					break;
				case "leftlittle":
					finger = "{\"fpg\":10}";
					convFile = new File(key + ".wsq");
					break;
				case "rightthumb":
					finger = "{\"fpg\":1}";
					convFile = new File(key + ".wsq");
					break;
				case "rightindex":
					finger = "{\"fpg\":2}";
					convFile = new File(key + ".wsq");
					break;
				case "rightmiddle":
					finger = "{\"fpg\":3}";
					convFile = new File(key + ".wsq");
					break;
				case "rightring":
					finger = "{\"fpg\":4}";
					convFile = new File(key + ".wsq");
					break;
				case "rightlittle":
					finger = "{\"fpg\":5}";
					convFile = new File(key + ".wsq");
					break;
				default:
					continue;
				}

				requestParam.setData(finger);
				logger.info(" Data: " + finger);

				byte[] decodeImg = Base64.getDecoder().decode(encodedImg);
				FileUtils.writeByteArrayToFile(convFile, decodeImg);

				requestParam.setFile(convFile);
				respKara = karalundiFingerervice.enrollMF(requestParam);

				String status = respKara.getStatus(); // FAIL, SUCCESS
				logger.info("Status: " + status);

				if (status != null && "SUCCESS".equals(respKara.getStatus().toUpperCase())) {
					logger.info("Usuario:" + operationId + "Enrolamiento exitoso de la huella");

				} else {
					logger.error("Ocurrió un error al enrolar la huella usuario:" + operationId);
					operationResult.setResultOK(false);
					operationResult.setErrorMessage("20001");
					return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
				}
			}
			operationResult.setResultOK(true);
			operationResult.setMessage("Se han almacenado las huellas en el expediente");
			return new ResponseEntity<>(operationResult, HttpStatus.OK);

		} catch (Exception ex) {
			logger.error("Ocurrió un error al parsear el json", ex);
			operationResult.setResultOK(false);
			operationResult.setErrorMessage("20001");
			return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/verify", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
	public ResponseEntity<OperationResult> sendValidationMinucias(HttpServletRequest request,
			@RequestPart(value = "json") String jsonRequest) {
		OperationResult operationResult = new OperationResult();
		try {
			JSONObject jsonString = new JSONObject(jsonRequest);
			String operationId = jsonString.getString("operationId");
			logger.info(" OperationId: " + operationId);

			ResponseKaralundi respKara = karalundiFingerervice.startSession("config_8", "5bd0917354a78", "");
			logger.info(" Session: " + respKara.getSession());

			RequestKaralundi requestParam = new RequestKaralundi();
			requestParam.setSession(respKara.getSession());
			requestParam.setSpeaker(operationId);

			Map<String,File> wsqFiles = new HashMap();
			@SuppressWarnings("unchecked")
			Iterator<String> iterator = jsonString.keys();

			while (iterator.hasNext()) {
				File convFile = null;
				String key = iterator.next();
				String encodedImg = "";

				if (!jsonString.getString(key).isEmpty())
					encodedImg = jsonString.getString(key);
				else
					continue;

				switch (key) {
				case "left-thumb":
					convFile = new File(key + ".wsq");
					break;
				case "left-index":
					convFile = new File(key + ".wsq");
					break;
				case "left-middle":
					convFile = new File(key + ".wsq");
					break;
				case "left-ring":
					convFile = new File(key + ".wsq");
					break;
				case "left-little":
					convFile = new File(key + ".wsq");
					break;
				case "right-thumb":
					convFile = new File(key + ".wsq");
					break;
				case "right-index":
					convFile = new File(key + ".wsq");
					break;
				case "right-middle":
					convFile = new File(key + ".wsq");
					break;
				case "right-ring":
					convFile = new File(key + ".wsq");
					break;
				case "right-little":
					convFile = new File(key + ".wsq");
					break;
				default:
					continue;
				}

				byte[] decodeImg = Base64.getDecoder().decode(encodedImg);
				FileUtils.writeByteArrayToFile(convFile, decodeImg);
				wsqFiles.put(key,convFile);
			}

			requestParam.setMapFiles(wsqFiles);
			respKara = karalundiFingerervice.verifyMF(requestParam);
			
			String status = respKara.getStatus(); // FAIL, SUCCESS
			logger.info("Status: " + status);

			if (status != null && "SUCCESS".equals(respKara.getStatus().toUpperCase())) {
				
				if ("ACCEPTED".equals(respKara.getSpeakerOutcome())){
					logger.info("Usuario:" + operationId + "Verificación exitosa usuario:" + operationId);
					operationResult.setResultOK(true);
					operationResult.setMessage("Se han almacenado las huellas en el expediente");
					return new ResponseEntity<>(operationResult, HttpStatus.OK);
				}else {
					logger.error("Verificación rechazada" + "usuario:" + operationId);
					operationResult.setResultOK(false);
					operationResult.setErrorMessage("20001");
					return new ResponseEntity<>(operationResult, HttpStatus.NOT_FOUND);
				}
				
				

			} else {
				logger.error("Ocurrió un error al verificar" + "usuario:" + operationId);
				operationResult.setResultOK(false);
				operationResult.setErrorMessage("20001");
				return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
			}

		} catch (Exception ex) {
			logger.error("Ocurrió un error al parsear el json", ex);
			operationResult.setResultOK(false);
			operationResult.setErrorMessage("20001");
			return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
		}
	}
}
