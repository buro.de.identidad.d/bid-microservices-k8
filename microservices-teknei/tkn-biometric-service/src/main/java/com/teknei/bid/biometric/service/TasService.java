package com.teknei.bid.biometric.service;

import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

public interface TasService {

	public JSONObject createDocument(
			String operationId,
            String docType,
            String headDocs,
            String tailDocs,
            String bidNombre,
            String bidPrimerApellido,
            String bidSegundoApellido); 
	 
	 public JSONObject addFile(String documentId, String fileName, MultipartFile file, String contentType) throws Exception;
}
