package com.teknei.bid.biometric.controller.icar;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.context.ServletWebServerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.teknei.bid.biometric.controller.karalundi.KaralundiFingerController;
import com.teknei.bid.biometric.data.OperationResult;
import com.teknei.bid.biometric.service.icar.IcarManager;

@RestController
@RequestMapping(value = "/rest/v1/test/enrollment/ocr")
public class IcarOCRController implements ApplicationListener<ServletWebServerInitializedEvent>{

	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(KaralundiFingerController.class);

    @Autowired
    private IcarManager icarManager;
	
	@PreAuthorize("hasAuthority('role_user')")
    @RequestMapping(value = "/id", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<OperationResult> enroll(
            @RequestPart(value = "json") String json,
            @RequestPart(value = "file", required = false) List<MultipartFile> files){

    	logger.info("Enroll ----->>> JSON: "+json+ " file:"+files.size());
    	JSONObject jsonResult = null;
    	OperationResult operationResult = new OperationResult();
    	try {
	    	//JSONObject credentialsOperationRequestJSON = new JSONObject(json);
	        //Long operationId = credentialsOperationRequestJSON.getLong("operationId");
	        //logger.info("OperationId: " +operationId);
	        
	        jsonResult = icarManager.getDocumentInfo(files, "image/jpeg", 500);
    	} catch (Exception ex) {
    		logger.error("Ocurrió un error en el servicio de finger enroll", ex);
    	}

    	// logger.info("jsonResult: {}", jsonResult);
        if (jsonResult == null) {
            operationResult.setResultOK(Boolean.FALSE);
            operationResult.setErrorMessage("10001");
            return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
        }

        try {
			String code = jsonResult.getString("code");

			operationResult.setErrorMessage(jsonResult.toString());
			operationResult.setResultOK(Boolean.TRUE);
			return new ResponseEntity<OperationResult>(operationResult, HttpStatus.OK);
			
			/*
			if("OK".equals(code) || "NOT_CORRECT".equals(code)) {
				operationResult.setErrorMessage(jsonResult.toString());
				operationResult.setResultOK(Boolean.TRUE);
				return new ResponseEntity<OperationResult>(operationResult, HttpStatus.OK);							
			}else {
				operationResult.setErrorMessage("No se pudo obtener información de la imagen");
				operationResult.setResultOK(Boolean.FALSE);
				return new ResponseEntity<OperationResult>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);				
			}*/
		} catch (JSONException jsonex) {
			logger.error("Ocurrió un error al obtener el code de la respuesta", jsonex);
			operationResult.setErrorMessage("Ocurrió un error en la respuesta");
			operationResult.setResultOK(Boolean.FALSE);
			return new ResponseEntity<OperationResult>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
		}
    }
	
	@Override
	public void onApplicationEvent(ServletWebServerInitializedEvent event) {
	}

}
