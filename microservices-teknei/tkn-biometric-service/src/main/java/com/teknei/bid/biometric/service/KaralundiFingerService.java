package com.teknei.bid.biometric.service;

import com.teknei.bid.biometric.data.karalundi.RequestKaralundi;
import com.teknei.bid.biometric.data.karalundi.ResponseKaralundi;

public interface KaralundiFingerService {

	ResponseKaralundi startSession(String configuration, String enterpriseId, String device);
	ResponseKaralundi endSession(RequestKaralundi request);
	ResponseKaralundi isTrained(RequestKaralundi request);
	ResponseKaralundi enrollMF(RequestKaralundi request);
	ResponseKaralundi disenroll(RequestKaralundi request);
	ResponseKaralundi deleteSegments(RequestKaralundi request);
	ResponseKaralundi verifyMF(RequestKaralundi request);
	ResponseKaralundi identifyMF(RequestKaralundi request);
	ResponseKaralundi updateMF(RequestKaralundi request);
	
}
