CREATE TABLE `bid_client` (
  `id_client` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(25) NOT NULL,
  `rfc` varchar(50) NOT NULL,
  `curp` varchar(50) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_client`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8

-- insert
insert into bid_client(name, type, rfc, curp) values ('Cliente 01', 'tipo 01', 'RFCCliente01', 'CURPCliente01');
insert into bid_client(name, type, rfc, curp) values ('Cliente 02', 'tipo 02', 'RFCCliente02', 'CURPCliente02');
insert into bid_client(name, type, rfc, curp) values ('Cliente 03', 'tipo 03', 'RFCCliente03', 'CURPCliente03');
insert into bid_client(name, type, rfc, curp) values ('Cliente 04', 'tipo 04', 'RFCCliente04', 'CURPCliente04');
insert into bid_client(name, type, rfc, curp) values ('Cliente 05', 'tipo 05', 'RFCCliente05', 'CURPCliente05');
insert into bid_client(name, type, rfc, curp) values ('Cliente 06', 'tipo 06', 'RFCCliente06', 'CURPCliente06');
insert into bid_client(name, type, rfc, curp) values ('Cliente 07', 'tipo 07', 'RFCCliente07', 'CURPCliente07');
insert into bid_client(name, type, rfc, curp) values ('Cliente 08', 'tipo 08', 'RFCCliente08', 'CURPCliente08');
insert into bid_client(name, type, rfc, curp) values ('Cliente 09', 'tipo 09', 'RFCCliente09', 'CURPCliente09');
insert into bid_client(name, type, rfc, curp) values ('Cliente 10', 'tipo 10', 'RFCCliente10', 'CURPCliente10');
